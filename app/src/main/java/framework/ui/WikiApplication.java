package framework.ui;

import android.app.Application;
import android.content.Context;

public class WikiApplication extends Application {

    private static Application application;
    private static Context appContext;

    /**
     * Return the application context {@link Application#getApplicationContext()}. This should not be used for any UI
     * operations. For UI operation use {@link UIService#getActivity()}
     *
     * @return {@link Context}
     */
    public static Context getAppContext() {
        return appContext;
    }

    public static Application getApplication() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(AppStateListener.getInstance());
        application = this;
        appContext = getApplicationContext();
    }
}