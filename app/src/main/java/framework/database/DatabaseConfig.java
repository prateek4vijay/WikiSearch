package framework.database;

import java.util.HashMap;

public interface DatabaseConfig {

    String getDatabaseName();

    int getDatabaseVersion();

    HashMap<Class<? extends DatabaseObject>, String> getTableMapping();
}