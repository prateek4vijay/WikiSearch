package framework.util;

public interface LoggerImpl {
    void logMessage(int level, String tag, String msg, Throwable throwable);
}