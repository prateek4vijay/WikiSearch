package framework.util;

import com.example.android.wikisearch.BaseFragment;

public class NotificationTarget {

    private Class<? extends BaseFragment> targetClass;
    private Object extraData;

    public NotificationTarget(Class<? extends BaseFragment> cls, Object data) {
        this.targetClass = cls;
        this.extraData = data;
    }

    public Class<? extends BaseFragment> getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class<? extends BaseFragment> cls) {
        this.targetClass = cls;
    }

    public Object getExtraData() {
        return extraData;
    }

    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }

    @Override
    public String toString() {
        return "NotificationTarget{" +
                "targetClass=" + targetClass +
                ", extraData=" + extraData +
                '}';
    }
}