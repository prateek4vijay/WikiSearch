package framework.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.android.wikisearch.BuildConfig;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileWriter;
import java.util.Locale;

import framework.ui.WikiApplication;

public final class Logger extends HandlerThread implements LoggerImpl {
    public static final String LOG_DIR = "/logs/";
    public static final String LOG_FILE_PREFIX = "WikiApp_";
    public static final String LOG_FILE_SUFFIX = ".log";
    public static final int DEFAULT_LOG_LEVEL = LOG_LEVEL_DEF.ERROR.value + LOG_LEVEL_DEF.WARN.value + LOG_LEVEL_DEF.INFO.value + LOG_LEVEL_DEF.DEBUG.value;
    private static final String FILE_NAME_PATTERN = "yyyy-MM-dd_HH";
    private static final int KEEP_LOG_FOR_DAYS = 5;
    private static Logger instance;
    private int LOG_LEVEL;
    private long logEndDate;
    private boolean isSettingModified = false;
    private Handler logHandler;
    private File logFile;
    private DateTime currentFileDate;
    private boolean errorDuringFileSetup = false;

    private FileWriter fileWriter;

    private Logger() {
        super("Logger");
        setupLogFile();
        start();
        logHandler = new Handler(getLooper());
    }

    public synchronized static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }

        return instance;
    }

    public static void i(String tag, String msg) {
        getInstance().logMessage(Log.INFO, tag, msg, null);
    }

    public static void i(String tag, String msg, Throwable error) {
        getInstance().logMessage(Log.INFO, tag, msg, error);
    }

    public static void d(String tag, String msg) {
        getInstance().logMessage(Log.DEBUG, tag, msg, null);
    }

    public static void d(String tag, String msg, Throwable error) {
        getInstance().logMessage(Log.DEBUG, tag, msg, error);
    }

    public static void e(String tag, String msg) {
        getInstance().logMessage(Log.ERROR, tag, msg, null);
    }

    public static void e(String tag, String msg, Throwable error) {
        getInstance().logMessage(Log.ERROR, tag, msg, error);
    }

    public static void w(String tag, String msg) {
        getInstance().logMessage(Log.WARN, tag, msg, null);
    }

    public static void w(String tag, String msg, Throwable error) {
        getInstance().logMessage(Log.WARN, tag, msg, error);
    }

    public int getLogLevel() {
        return LOG_LEVEL;
    }

    public void setLogLevel(int level) {
        LOG_LEVEL = level;
    }


    @SuppressWarnings("unused")
    public boolean isDebugEnabled() {
        return canLog(LOG_LEVEL_DEF.DEBUG);
    }

    private boolean canLog(LOG_LEVEL_DEF levelDef) {
        if (isSettingModified && (logEndDate - System.currentTimeMillis()) < 0) {
            LOG_LEVEL = DEFAULT_LOG_LEVEL;
            logEndDate = -1L;
            isSettingModified = false;
        }

        return ((LOG_LEVEL & levelDef.value) == levelDef.value);
    }

    public void logMessage(final int level, final String tag, final String msg,
                           final Throwable throwable) {
        logHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    logInternal(level, tag, msg, throwable);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    System.out.println("Received out of memory while processing logs.");
                }
            }
        });
    }

    private void logInternal(int level, String tag, String msg, Throwable throwable) {
        if (!canLog(LOG_LEVEL_DEF.getLevelDef(level)))
            return;

        String levelIdentifier = "I";
        switch (LOG_LEVEL_DEF.getLevelDef(level)) {
            case INFO:
                //if (BuildConfig.DEBUG)
                Log.i(tag, msg, throwable);
                levelIdentifier = "I";
                break;
            case WARN:
                //if (BuildConfig.DEBUG)
                Log.w(tag, msg, throwable);
                levelIdentifier = "W";
                break;
            case ERROR:
                //if (BuildConfig.DEBUG)
                Log.e(tag, msg, throwable);
                levelIdentifier = "E";
                break;
            case DEBUG:
                //if (BuildConfig.DEBUG)
                Log.d(tag, msg, throwable);
                levelIdentifier = "D";
                break;
        }

        StringBuilder msgBuilder = new StringBuilder();
        msgBuilder.append("[").append(DateTime.now().toString("yyyy-MM-dd HH:mm:ss:SSS"))
                .append("]");
        msgBuilder.append("[").append(levelIdentifier).append("]");
        msgBuilder.append("[").append(tag).append("] ");
        msgBuilder.append(msg);

        if (throwable != null) {
            msgBuilder.append("\n").append(Log.getStackTraceString(throwable));
        }

        logToFile(msgBuilder.toString());
    }

    private void setupLogFile() {
        try {
            //Close the write if its open
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (Exception e) {
                    //Ignore Exception.
                }
                fileWriter = null;
            }

            File logDir = new File(WikiApplication.getAppContext().getExternalFilesDir(null), LOG_DIR);
            if (!logDir.exists()) {
                boolean dirCreated = logDir.mkdirs();
                if (!dirCreated) {
                    return;
                }
            }

            DateTime maxOldFileTime = DateTime.now().withTime(0, 0, 0, 0).minusDays(KEEP_LOG_FOR_DAYS);
            //Check for the maz size of file and the current file...
            File[] files = logDir.listFiles();
            if (files != null && files.length > 0) {
                for (File file : files) {
                    if (file == null || !file.isFile()) {
                        continue;
                    }

                    try {
                        if (maxOldFileTime.isAfter(new DateTime(file.lastModified()).withTime(0, 0, 0, 0))) {
                            file.delete();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            currentFileDate = DateTime.now();
            logFile = new File(logDir, LOG_FILE_PREFIX + currentFileDate.toString(FILE_NAME_PATTERN, Locale.US) + LOG_FILE_SUFFIX);
            if (!logFile.exists()) {
                logFile.createNewFile();
            }

            if (logFile != null) {
                try {
                    if (fileWriter == null) {
                        fileWriter = new FileWriter(logFile, true);
                    }

                    fileWriter.append("-------------------------------------------------------------");
                    fileWriter.append("\n");
                    fileWriter.append("App Version : " + BuildConfig.VERSION_NAME);
                    fileWriter.append("\n");
                    fileWriter.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            errorDuringFileSetup = false;
        } catch (Exception e) {
            errorDuringFileSetup = true;

            e.printStackTrace();
        }
    }

    private void logToFile(String msg) {
        if (!errorDuringFileSetup && (logFile == null || !logFile.exists())) {
            setupLogFile();
        } else if (currentFileDate != null) {
            DateTime dateTime = DateTime.now();
            if (dateTime.getHourOfDay() != currentFileDate.getHourOfDay()
                    || dateTime.getDayOfMonth() != currentFileDate.getDayOfMonth()
                    || dateTime.getMonthOfYear() != currentFileDate.getMonthOfYear()
                    || dateTime.getYear() != currentFileDate.getYear()) {
                setupLogFile();
            }
        } else {
            setupLogFile();
        }

        if (logFile != null && !errorDuringFileSetup && logFile.exists()) {
            try {
                if (fileWriter == null) {
                    fileWriter = new FileWriter(logFile);
                }

                fileWriter.append(msg);
                fileWriter.append("\n");
                fileWriter.flush();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }

    public enum LOG_LEVEL_DEF {
        ERROR(1),
        WARN(2),
        INFO(4),
        DEBUG(8);

        public final int value;

        LOG_LEVEL_DEF(int level) {
            this.value = level;
        }

        private static LOG_LEVEL_DEF getLevelDef(int level) {
            switch (level) {
                case Log.DEBUG:
                    return DEBUG;
                case Log.ERROR:
                    return ERROR;
                case Log.WARN:
                    return WARN;
                default:
                    return INFO;
            }
        }

        @Override
        public String toString() {
            return name();
        }
    }
}