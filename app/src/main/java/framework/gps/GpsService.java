package framework.gps;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.example.android.wikisearch.R;
import com.example.android.wikisearch.utils.FrameworkUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.concurrent.CopyOnWriteArraySet;

import framework.ui.AppStateListener;
import framework.ui.UIService;
import framework.ui.WikiApplication;
import framework.util.Logger;
import framework.util.PermissionHelper;

public class GpsService extends HandlerThread {

    public final static int PLAY_SERVICE_GPS_REQUEST_CODE = 29842;
    public final static String PLAY_SERVICE_GPS_REQUEST_INTENT_KEY = "GPS_ERROR_KEY";

    private static final String TAG = "GpsService";
    private static final Object LOCK = new Object();

    private static GpsService instance = null;
    private CopyOnWriteArraySet<GpsListener> gpsListeners;
    private Looper mLooper;

    private SettingsClient mSettingsClient;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;

    private boolean isRequestingUpdates;
    private boolean isLocationCheckRunning = false;


    private GpsService() {
        super("GpsService");
        gpsListeners = new CopyOnWriteArraySet<>();
        start();
        //Call this so its blocked until looper is created.
        mLooper = getLooper();
    }

    public static GpsService getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new GpsService();
            }
        }

        return instance;
    }

    public void cleanup() {
        synchronized (LOCK) {
            endUpdates();
            quit();
            instance = null;
        }
    }

    public void addGpsListener(GpsListener listener) {
        synchronized (LOCK) {
            if (listener == null)
                return;

            gpsListeners.add(listener);
            if (!gpsListeners.isEmpty()) {
                startGpsUpdates();
            }
        }
    }

    public void removeGpsListener(GpsListener listener) {
        synchronized (LOCK) {
            if (listener == null)
                return;

            gpsListeners.remove(listener);
            if (gpsListeners.isEmpty())
                endUpdates();
        }
    }

    private void startGpsUpdates() {
        synchronized (LOCK) {
            Logger.d(TAG, "Starting the service");
            if (!FrameworkUtils.checkPlayServiceAvailability(PLAY_SERVICE_GPS_REQUEST_CODE)) {
                Logger.d(TAG, "Play service is not available.");
                return;
            }

            if (mSettingsClient == null) {
                mSettingsClient = LocationServices.getSettingsClient(WikiApplication.getAppContext());
            }

            if (mFusedLocationProviderClient == null) {
                mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(WikiApplication.getAppContext());
            }

            if (mLocationRequest == null) {
                // Google Play service is available let request for the Locations..
                mLocationRequest = LocationRequest.create();
                // Right now get every 5second.
                mLocationRequest.setInterval(5000L);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                mLocationRequest.setFastestInterval(1000L);
            }

            if (mLocationSettingsRequest == null) {
                mLocationSettingsRequest = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest).setAlwaysShow(true).build();
            }

            if (mLocationCallback == null) {
                createLocationCallback();
            }

            if (!isRequestingUpdates) {
                Logger.d(TAG, "LocationUpdate is not running. Checking for Location Settings.");
                checkLocationSettings();
            }
        }
    }

    public void onPermissionGranted() {
        startGpsUpdates();
    }

    public void onPermissionDenied() {
        sendError("Permission Denied");
        endUpdates();
    }

    private void checkLocationSettings() {
        if (isLocationCheckRunning)
            return;

        isLocationCheckRunning = true;

        Logger.d(TAG, "Check Location settings start.");
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Logger.d(TAG, "All settings sees correct. Start getting location updates.");
                        beginUpdates();
                        isLocationCheckRunning = false;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        Logger.d(TAG, "Location Service Status." + statusCode);
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                // Location settings are not satisfied. But could be fixed by showing the user
                                // a dialog.
                                Logger.d(TAG, "Location Service Error, Resolution Required.");
                                Activity activity = UIService.getInstance().getActivity();
                                if (UIService.getInstance().getActivity() != null && AppStateListener.isAppVisible()) {
                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(activity, PLAY_SERVICE_GPS_REQUEST_CODE);
                                    } catch (Exception ee) {
                                        Logger.e(TAG, "Location Service Error, Can't show dialog", ee);
                                        sendError("Error in Location Settings. Code : " + statusCode);
                                    }
                                } else {
                                    Logger.e(TAG, "Location Service Error, Activity is null.");
                                    Intent intent = WikiApplication.getAppContext().getPackageManager().getLaunchIntentForPackage(WikiApplication.getAppContext().getPackageName());
                                    if (intent != null) {
                                        try {
                                            intent.setAction(Intent.ACTION_MAIN);
                                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                            intent.putExtra("ACTION", PLAY_SERVICE_GPS_REQUEST_CODE);
                                            intent.putExtra(PLAY_SERVICE_GPS_REQUEST_INTENT_KEY, ((ResolvableApiException) e).getResolution());
                                            PendingIntent pendingIntent = PendingIntent.getActivity(
                                                    WikiApplication.getAppContext(),
                                                    PLAY_SERVICE_GPS_REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                                            NotificationCompat.Builder builder = new NotificationCompat.Builder(WikiApplication.getAppContext());
                                            builder.setAutoCancel(true);
                                            builder.setSmallIcon(R.drawable.ic_notification);
                                            builder.setContentTitle(WikiApplication.getAppContext().getString(R.string.location_services_setting_fix_title));
                                            builder.setContentText(WikiApplication.getAppContext().getString(R.string.location_services_setting_fix_message));
                                            builder.setContentIntent(pendingIntent);

                                            ((NotificationManager) WikiApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE)).notify(0, builder.build());
                                        } catch (Exception ee) {
                                            Logger.e(TAG, "Error in showing location setting recoverable notification.", ee);
                                        }
                                    }
                                    sendError("Error in Location Settings. Code : " + statusCode);
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Logger.e(TAG, "Location Service Error, Resolution not found.");
                                sendError("Error in Location Settings, Change Unavailable. Code : " + statusCode);
                                break;
                        }
                        isLocationCheckRunning = false;
                    }
                });
    }

    @SuppressWarnings("MissingPermission")
    private void beginUpdates() {
        Logger.d(TAG, "beginUpdates Start.");
        if (!isRequestingUpdates) {
            Logger.d(TAG, "beginUpdates condition matched going to request.");
            //Check if we have the GPS permission
            try {
                if (PermissionHelper.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        || PermissionHelper.hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, mLooper)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Logger.d(TAG, "beginUpdates update request was succesfull.");
                                    isRequestingUpdates = true;
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Logger.d(TAG, "beginUpdates update request failed.");
                                    isRequestingUpdates = false;
                                }
                            });
                } else {
                    Logger.e(TAG, "GPS service doesn't has the permission.");
                    sendError("Location permission denied.");
                    PermissionHelper.requestPermission(PLAY_SERVICE_GPS_REQUEST_CODE, R.string.permission_ds_notification_msg,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION);
                    isRequestingUpdates = false;
                }
            } catch (Exception e) {
                Logger.e(TAG, "Error while starting location updates.", e);
                isRequestingUpdates = false;
            }
        }
    }

    private void endUpdates() {
        synchronized (LOCK) {
            Logger.d(TAG, "Stop the service.");
            if (mFusedLocationProviderClient == null) {
                Logger.d(TAG, "Client is null return");
                return;
            }

            if (isRequestingUpdates) {
                Logger.d(TAG, "endUpdates condition matched going to end updates.");
                try {
                    mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Logger.d(TAG, "endUpdates done.");
                                    isRequestingUpdates = false;
                                }
                            });
                    isRequestingUpdates = false;
                } catch (Exception e) {
                    Logger.e(TAG, "Error while ending location updates.", e);
                }
            }
        }
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Logger.d(TAG, "Location Result received. " + locationResult);
                Location location = null;
                if (locationResult != null) {
                    location = locationResult.getLastLocation();
                }

                if (gpsListeners != null && !gpsListeners.isEmpty()) {
                    sendGpsPoint(location);
                } else {
                    endUpdates();
                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                if (locationAvailability != null && !locationAvailability.isLocationAvailable()) {
                    sendError("Location Unavailable");
                }
            }
        };
    }


    private void sendError(String err) {
        Logger.d(TAG, "Error occurred. " + err);
        if (gpsListeners != null) {
            for (GpsListener listener : gpsListeners) {
                listener.onError(err);
            }
        }
    }

    private void sendGpsPoint(Location location) {
        Logger.d(TAG, "Gps Point received. " + location);
        if (gpsListeners != null) {
            for (GpsListener listener : gpsListeners) {
                listener.onGpsPointReceived(location);
            }
        }
    }
}