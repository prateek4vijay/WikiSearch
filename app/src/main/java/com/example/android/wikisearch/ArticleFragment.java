package com.example.android.wikisearch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.example.android.wikisearch.utils.FrameworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import framework.util.Logger;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleFragment extends BaseFragment {

    public static final String TAG = ArticleFragment.class.getSimpleName();
    public static final String ARTICLE_NAME = "ARTICLE_NAME";
    public static final String ARTICLE_PAGE_ID = "ARTICLE_PAGE_ID";
    public static final String ACTION = "action";
    public static final String PROP = "prop";
    public static final String FORMAT = "format";
    public static final String RVPROP = "rvprop";
    public static final String RVSECTION = "rvsection";
    public static final String TITLES = "titles";
    public static final String QUERY = "query";
    public static final String EXTRACTS = "extracts";
    public static final String JSON = "json";
    public static final String CONTENT = "content";
    private WebView webView;
    private LoadingStatus PAGE_STATUS = LoadingStatus.UNKNOWN;
    private String articleName = "";
    private String articlePageid = "";
    private TextView placeholder_text;
    private OnFragmentInteractionListener mListener;

    public ArticleFragment() {

    }

    public static ArticleFragment getInstance(String articleName, String pageId) {
        ArticleFragment articleFragment = new ArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARTICLE_NAME, articleName);
        bundle.putString(ARTICLE_PAGE_ID, pageId);
        articleFragment.setArguments(bundle);
        return articleFragment;
    }

    @Override
    protected String getFragmentDisplayName() {
        return articleName;
    }

    @Override
    public void refreshData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_article, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            articleName = getArguments().getString(ARTICLE_NAME);
            articlePageid = getArguments().getString(ARTICLE_PAGE_ID);
        }
        if (mListener != null) {
            mListener.onFragmentInteraction(articleName);
        }
        webView = (WebView) view.findViewById(R.id.webview);
        placeholder_text = (TextView) view.findViewById(R.id.placeholder_text);
        WebSettings ws = webView.getSettings();
        ws.setJavaScriptEnabled(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadJSON(articleName);
    }

    private void loadJSON(String query) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put(ACTION, QUERY);
        queryMap.put(PROP, EXTRACTS);
        queryMap.put(FORMAT, JSON);
        queryMap.put(RVPROP, CONTENT);
        queryMap.put(RVSECTION, "0");
        queryMap.put(TITLES, query);
        Call<ResponseBody> call = FrameworkUtils.initRetrofit().getArticle(queryMap);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    ResponseBody jsonResponse = response.body();
                    if (jsonResponse != null) {
                        try {
                            parseJson(jsonResponse.string().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Logger.d("ArticleFragment", "response is :" + jsonResponse.toString());
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void parseJson(String jsonResponse) {
        try {
            JSONObject rootObject = new JSONObject(jsonResponse);
            JSONObject queryObject = rootObject.getJSONObject("query");
            if (queryObject != null) {
                JSONObject pagesObject = queryObject.getJSONObject("pages");
                if (pagesObject != null) {
                    JSONObject pagesObjectId = pagesObject.getJSONObject(articlePageid);
                    String extract = pagesObjectId.getString("extract");
                    if (Utils.isEmptyOrWhitespace(extract)) {
                        placeholder_text.setVisibility(View.VISIBLE);
                        webView.setVisibility(View.GONE);
                    } else {
                        placeholder_text.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                        loadPage(extract);
                    }

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadPage(String content) {

        if (!Utils.isDataConnectionOn()) {
            if (PAGE_STATUS != LoadingStatus.LOADED) {

            }
            return;
        }
        webView.loadData(content, "text/html", "UTF-8");
        Logger.d(TAG, "Page loading started.");
    }

    private enum LoadingStatus {
        UNKNOWN,
        LOADING,
        LOADED,
        ERROR;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String title);
    }
}

