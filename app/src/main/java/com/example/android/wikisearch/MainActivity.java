//package com.example.android.wikisearch;
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.SearchView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.widget.TextView;
//
//import com.example.android.wikisearch.utils.FrameworkUtils;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.annotations.NonNull;
//import io.reactivex.functions.Consumer;
//import io.reactivex.functions.Predicate;
//import io.reactivex.schedulers.Schedulers;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//public class MainActivity extends AppCompatActivity {
//
//    private RecyclerView mRecyclerView;
//    private DataAdapter mAdapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        initViews();
//    }
//
//    private void initViews() {
//        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
//        mRecyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(layoutManager);
//        mAdapter = new DataAdapter();
//        mRecyclerView.setAdapter(mAdapter);
//    }
//
//
//
//    private void loadJSON(String query) {
//
//        Map<String, String> queryMap = new HashMap<>();
//        queryMap.put("action", "query");
//        queryMap.put("format", "json");
//        queryMap.put("prop", "pageimages|pageterms");
//        queryMap.put("generator", "prefixsearch");
//        queryMap.put("redirects", "1");
//        queryMap.put("formatversion", "2");
//        queryMap.put("piprop", "thumbnail");
//        queryMap.put("pithumbsize", "100");
//        queryMap.put("wbptterms", "description");
//        queryMap.put("gpssearch", query);
//        queryMap.put("gpslimit", "10");
//        Call<WikiSearchByName> call = FrameworkUtils.initRetrofit().getSearchListByName(queryMap);
//        call.enqueue(new Callback<WikiSearchByName>() {
//            @Override
//            public void onResponse(Call<WikiSearchByName> call, Response<WikiSearchByName> response) {
//
//                Log.e("Response", "test");
//                if (response.body() != null) {
//                    WikiSearchByName jsonResponse = response.body();
//                    if (jsonResponse != null) {
//                        mAdapter.updateData(jsonResponse.getQuery().getPages());
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<WikiSearchByName> call, Throwable t) {
//                Log.d("Error", t.getMessage());
//            }
//        });
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        MenuItem searchItem = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) searchItem.getActionView();
//        rxJavaSearchwithDebounce(searchView);
//        return true;
//    }
//
//    private void rxJavaSearchwithDebounce(SearchView searchView) {
//        RxSearch.fromSearchView(searchView)
//                .debounce(300, TimeUnit.MILLISECONDS)
//                .filter(new Predicate<String>() {
//                    @Override
//                    public boolean test(String text) throws Exception {
//                        if (text.isEmpty()) {
//                            return false;
//                        } else {
//                            return true;
//                        }
//                    }
//                })
//                .distinctUntilChanged()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<String>() {
//                    @Override
//                    public void accept(@NonNull String query) throws Exception {
//                        loadJSON(query);
//                    }
//                });
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        return super.onOptionsItemSelected(item);
//    }
//}
