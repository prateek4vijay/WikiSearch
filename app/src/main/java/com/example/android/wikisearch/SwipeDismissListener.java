package com.example.android.wikisearch;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;

public class SwipeDismissListener implements OnTouchListener {

    private static final int ANIMATION_TIME = 500;
    private int mSlop;
    private int minFlingVelocity;
    private int maxFlingVelocity;
    private Rect viewHitRect;
    private int viewWidth = 1;
    private float downX;
    private float downY;
    private boolean isSwiping;
    private int swipingSlop;
    private VelocityTracker velocityTracker;
    private boolean isPaused;
    private float translationX;

    public SwipeDismissListener(Context context) {
        ViewConfiguration vc = ViewConfiguration.get(context);
        mSlop = vc.getScaledTouchSlop();
        minFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
        maxFlingVelocity = vc.getScaledMaximumFlingVelocity();
    }

    public void setEnabled(boolean enabled) {
        isPaused = !enabled;
    }

    @Override
    public boolean onTouch(final View swipeView, MotionEvent motionEvent) {
        // offset because the view is translated during swipe
        motionEvent.offsetLocation(translationX, 0);
        if (viewWidth < 2) {
            viewWidth = swipeView.getWidth();
            viewHitRect = new Rect();
            swipeView.getHitRect(viewHitRect);
        }

        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN: {
                if (isPaused) {
                    return false;
                }

                downX = motionEvent.getRawX();
                downY = motionEvent.getRawY();
                velocityTracker = VelocityTracker.obtain();
                velocityTracker.addMovement(motionEvent);
                return true;
            }
            case MotionEvent.ACTION_CANCEL: {
                if (velocityTracker == null) {
                    break;
                }

                if (isSwiping) {
                    // cancel
                    swipeView.animate().translationX(0).alpha(1).setDuration(ANIMATION_TIME).setListener(null);
                }

                velocityTracker.recycle();
                velocityTracker = null;
                downX = 0;
                downY = 0;
                translationX = 0;
                isSwiping = false;
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (velocityTracker == null) {
                    break;
                }

                if (!isSwiping && viewHitRect != null
                        && viewHitRect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    swipeView.performClick();
                }

                float deltaX = motionEvent.getRawX() - downX;
                velocityTracker.addMovement(motionEvent);
                velocityTracker.computeCurrentVelocity(1000);
                float velocityX = velocityTracker.getXVelocity();
                float absVelocityX = Math.abs(velocityX);
                float absVelocityY = Math.abs(velocityTracker.getYVelocity());
                boolean dismiss = false;
                boolean dismissRight = false;
                if (Math.abs(deltaX) > viewWidth / 2 && isSwiping) {
                    dismiss = true;
                    dismissRight = deltaX > 0;
                } else if (minFlingVelocity <= absVelocityX && absVelocityX <= maxFlingVelocity && absVelocityY < absVelocityX
                        && isSwiping) {
                    // dismiss only if flinging in the same direction as dragging
                    dismiss = (velocityX < 0) == (deltaX < 0);
                    dismissRight = velocityTracker.getXVelocity() > 0;
                }

                if (dismiss) {
                    float pixelLeft = viewWidth - Math.abs(translationX);
                    long animationTime = (long) ((pixelLeft / absVelocityX) * 1000);

                    // dismiss
                    swipeView.animate().translationX(dismissRight ? viewWidth : -viewWidth).alpha(0)
                            .setDuration(Math.min(animationTime, ANIMATION_TIME)).setListener(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            performDismiss(swipeView);
                        }
                    });
                } else {
                    // cancel
                    swipeView.animate().translationX(0).alpha(1).setDuration(ANIMATION_TIME).setListener(null);
                }

                velocityTracker.recycle();
                velocityTracker = null;
                downX = 0;
                downY = 0;
                translationX = 0;
                isSwiping = false;
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (velocityTracker == null || isPaused) {
                    break;
                }

                velocityTracker.addMovement(motionEvent);
                float deltaX = motionEvent.getRawX() - downX;
                float deltaY = motionEvent.getRawY() - downY;
                if (Math.abs(deltaX) > mSlop && Math.abs(deltaY) < Math.abs(deltaX) / 2) {
                    isSwiping = true;
                    swipingSlop = (deltaX > 0 ? mSlop : -mSlop);
                }

                if (isSwiping) {
                    translationX = deltaX;
                    swipeView.setTranslationX(deltaX - swipingSlop);
                    swipeView.setAlpha(Math.max(0f, Math.min(1f, 1f - (Math.abs(deltaX) / viewWidth))));
                    return true;
                }
                break;
            }
        }
        return false;
    }

    private void performDismiss(final View swipeView) {
        swipeView.setVisibility(View.GONE);
        swipeView.setTranslationX(0);
        swipeView.setAlpha(1f);
    }
}
