package com.example.android.wikisearch;

import android.view.View;

public interface RecyclerViewClickListener {

    void onClick(View view, int position);
}