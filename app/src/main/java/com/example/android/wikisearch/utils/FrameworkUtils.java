package com.example.android.wikisearch.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;

import com.example.android.wikisearch.R;
import com.example.android.wikisearch.RequestInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;

import framework.ui.AppStateListener;
import framework.ui.UIService;
import framework.ui.WikiApplication;
import framework.util.Logger;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FrameworkUtils {

    public static final String BASE_URL = "https://en.wikipedia.org/";
    static RequestInterface request;

    public static String serializeObject(Object obj) {
        if (obj == null)
            return null;

        return new Gson().toJson(obj);
    }

    public static <T> T deserializeObject(String data, Class<T> cls) {
        if (isEmptyOrWhitespace(data))
            return null;

        try {
            return new Gson().fromJson(data, cls);
        } catch (Exception e) {
            Logger.e("GpsAddress", "Error while deserializing the address", e);
            return null;
        }
    }

    public static boolean checkPlayService(Activity activity, int requestCode) {
        return checkPlayServiceAvailability(activity, requestCode);
    }

    public static boolean checkPlayServiceAvailability(int requestCode) {
        return checkPlayServiceAvailability(null, requestCode);
    }

    public static boolean checkPlayServiceAvailability(Activity activity, int requestCode) {
        final Activity localActivity;
        if (activity != null) {
            localActivity = activity;
        } else {
            localActivity = UIService.getInstance().getActivity();
        }

        int errorCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(WikiApplication.getAppContext());
        if (errorCode != ConnectionResult.SUCCESS) {
            Logger.e("FrameworkUtils", "Google Play Service is not available. Error Code : " + errorCode);
            try {
                if (localActivity != null && AppStateListener.isAppVisible()) {
                    if (GoogleApiAvailability.getInstance().isUserResolvableError(errorCode)) {
                        Logger.e("FrameworkUtils", "Google Play Service is not available. Error is UserRecoverable. Showing prompt to the user.");
                        if (localActivity instanceof FragmentActivity) {
                            GoogleApiAvailability.getInstance().showErrorDialogFragment(activity, errorCode, requestCode, new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    Logger.e("FrameworkUtils", "User Cancelled the Play Service action. Lets Finish the activity.");
                                    //Finish the activity here..
                                    UIService.getInstance().showAlert(R.string.error, R.string.play_service_required_msg, R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                }
                            });
                        } else {
                            GoogleApiAvailability.getInstance().getErrorDialog(activity, errorCode, requestCode, new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    Logger.e("FrameworkUtils", "User Cancelled the Play Service action. Lets Finish the activity.");
                                    //Finish the activity here..
                                    UIService.getInstance().showAlert(R.string.error, R.string.play_service_required_msg, R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                }
                            }).show();
                        }
                    } else {
                        Logger.e("FrameworkUtils", "Google Play Service is not available. Error is not UserRecoverable. Killing the app");
                        UIService.getInstance().showAlert(R.string.error, R.string.play_service_required_msg, R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                    }
                } else {
                    GoogleApiAvailability.getInstance().showErrorNotification(WikiApplication.getAppContext(), errorCode);
                }
            } catch (Exception e) {
                Logger.e("FrameworkUtils", "Error while showing error notification for play service availability.", e);
            }

            return false;
        } else {
            Logger.i("FrameworkUtils", "PlayService is available on device.");
        }
        return true;
    }

    public static boolean isEmptyOrWhitespace(String string) {
        if (string == null || "".equals(string.trim())) {
            return true;
        }

        return false;
    }

    public static boolean parseBoolean(String data) {
        if (data == null || "".equals(data.trim())) {
            return false;
        }

        if ("0".equals(data.trim()) || "false".equalsIgnoreCase(data.trim())) {
            return false;
        }

        return true;
    }

    public static RequestInterface initRetrofit() {
        if (request == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            request = retrofit.create(RequestInterface.class);
        }
        return request;

    }
}
