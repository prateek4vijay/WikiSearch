package com.example.android.wikisearch.utils;

public class AppConstants {

    public static final String ACTION = "action";
    public static final String FORMAT = "format";
    public static final String PROP = "prop";
    public static final String GENERATOR = "generator";
    public static final String REDIRECTS = "redirects";
    public static final String FORMATVERSION = "formatversion";
    public static final String PIPROP = "piprop";
    public static final String PITHUMBSIZE = "pithumbsize";
    public static final String WBPTTERMS = "wbptterms";
    public static final String GPSSEARCH = "gpssearch";
    public static final String GPSLIMIT = "gpslimit";
    public static final String QUERY = "query";
    public static final String JSON = "json";
    public static final String PAGEIMAGES_PAGETERMS = "pageimages|pageterms";
    public static final String PREFIXSEARCH = "prefixsearch";
    public static final String REDIRECTSVALUE = "1";
    public static final String FORMATVERSIONVALUE = "2";
    public static final String THUMBNAIL = "thumbnail";
    public static final String PITHUMBSIZE_VALUE = "100";
    public static final String DESCRIPTION = "description";
    public static final String GPSLIMIT_VALUE = "10";
    public static final String LIST = "list";
    public static final String GEOSEARCH = "geosearch";
    public static final String GSCOORD = "gscoord";
    public static final String GSRADIUS = "gsradius";
    public static final String GSRADIUSVALUE = "10000";
    public static final String GSLIMIT = "gslimit";
    public static final String GSlimitVALUE = "10";
}
