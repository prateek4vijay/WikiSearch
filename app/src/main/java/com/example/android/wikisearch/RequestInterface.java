package com.example.android.wikisearch;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RequestInterface {

    @GET("/w/api.php")
    Call<WikiSearchByName> getSearchListByName(@QueryMap Map<String, String> options);

    @GET("/w/api.php")
    Call<WikiSearchByLocation> getSearchListByLocation(@QueryMap Map<String, String> options);

    @GET("/w/api.php")
    Call<ResponseBody> getArticle(@QueryMap Map<String, String> options);

}
