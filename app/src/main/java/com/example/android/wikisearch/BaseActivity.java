package com.example.android.wikisearch;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.wikisearch.utils.FrameworkUtils;

import framework.ui.AppStateListener;
import framework.ui.UIProtocol;
import framework.ui.UIService;
import framework.util.Logger;
import framework.util.NotificationTarget;
import framework.util.PermissionHelper;

@SuppressLint({"InflateParams", "ClickableViewAccessibility"})
public class BaseActivity extends UIProtocol implements OnClickListener, ArticleFragment.OnFragmentInteractionListener {
    private View rootView;
    private LinearLayout navigationBar;
    private TextView navTitleView;
    private LinearLayout navRightButton;
    private ImageView navLeftButton;

    private View errorView;
    private ImageView errorImageView;
    private TextView errorTextView;
    private TextView errorButton;
    private CountDownTimer inAppErrorTimer;

    private boolean handlePermissionOnResume = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = getLayoutInflater().inflate(R.layout.activity_base, null);
        navigationBar = rootView.findViewById(R.id.home_nav_bar);
        navTitleView = rootView.findViewById(R.id.home_title);
        navTitleView.setSelected(true);
        navLeftButton = rootView.findViewById(R.id.home_left_nav_button);
        navLeftButton.setOnTouchListener(Utils.getImageViewStateTouchListenr());
        navLeftButton.setOnClickListener(this);

        navRightButton = rootView.findViewById(R.id.home_right_nav_button);

        errorView = rootView.findViewById(R.id.home_error_view);
        errorView.setOnTouchListener(new SwipeDismissListener(this));
        errorImageView = rootView.findViewById(R.id.home_error_icon);
        errorTextView = rootView.findViewById(R.id.home_error_text);
        errorButton = rootView.findViewById(R.id.home_error_btn);

        setContentView(rootView);

        // Lets clear any back stack. Even if activity is re-launching we should start fresh..
        UIService.getInstance().clearBackStack();
        HomeFragment loginFragment = new HomeFragment();
        UIService.getInstance().addFragment(loginFragment, true);
    }

    @Override
    protected void onDestroy() {
        try {
            //Dismiss all alert when we are getting killed
            UIService.getInstance().dismissAllAlert();
        } catch (Exception e) {
            Logger.e("BaseActivity", "Error while dismissing all dialogs.", e);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FrameworkUtils.checkPlayServiceAvailability(this, -1);
        if (handlePermissionOnResume) {
            handlePermissionIntent(getIntent());
            handlePermissionOnResume = false;
        }
        navigationBar.setVisibility(View.GONE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent, false);
    }

    private NotificationTarget handleIntent(Intent intent, boolean isFromOnCreate) {
        if (PermissionHelper.isPermissionRequestIntent(intent)) {
            handlePermissionIntent(intent);
            return null;
        }
        handlePermissionOnResume = false;
        return null;
    }

    private void handlePermissionIntent(Intent intent) {
        if (intent == null || !PermissionHelper.isPermissionRequestIntent(intent)) {
            return;
        }

        int action = intent.getIntExtra(PermissionHelper.PERMISSION_TYPE_CODE, -1);
        int requestCode = intent.getIntExtra(PermissionHelper.PERMISSION_REQUEST_CODE, -1);
        String[] permissions = intent.getStringArrayExtra(PermissionHelper.PERMISSION_ARRAY);
        int messageId = intent.getIntExtra(PermissionHelper.PERMISSION_MESSAGE_ID, -1);

        if (!AppStateListener.isAppVisible()) {
            handlePermissionOnResume = true;
            return;
        }

        handlePermissionOnResume = false;
        switch (action) {
            case PermissionHelper.REQUEST_PERMISSION_ACTION:
                if (requestCode == -1 || permissions == null || permissions.length == 0) {
                    return;
                }

                PermissionHelper.requestPermission(this, requestCode, messageId,
                        R.string.permission_blocked_general, permissions);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        BaseFragment baseFragment = UIService.getInstance().getActiveFragment();
        if (baseFragment != null && baseFragment.canHandleBack()) {
            baseFragment.onBackPressed();
        } else if (UIService.getInstance().getBackStackFragmentsCount() <= 1) {
            moveTaskToBack(true);
        } else {
            super.onBackPressed();
            showLeftNavButton();
        }
    }

    public int getFragmentContainerId() {
        return R.id.home_fragment_container;
    }

    @Override
    public void setNavBarVisibility(boolean visible) {
        if (visible)
            navigationBar.setVisibility(View.VISIBLE);
        else
            navigationBar.setVisibility(View.GONE);
    }

    @Override
    public void showLeftNavButton() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navLeftButton.setVisibility((UIService.getInstance()
                        .getBackStackFragmentsCount() > 1) ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    @Override
    public void hideLeftNavButton() {
        navLeftButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showRightNavButton(View view, OnClickListener clickListener) {
        // Remove any previous views..
        navRightButton.removeAllViews();
        navRightButton.setOnClickListener(null);
        if (view == null) {
            return;
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        navRightButton.addView(view, params);
        view.setOnClickListener(clickListener);
        navRightButton.setOnClickListener(clickListener);
        navRightButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRightNavButton() {
        navRightButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setTitle(String title) {
        if (Utils.isEmptyOrWhitespace(title))
            title = getString(R.string.app_name);

        navTitleView.setText(title);
    }

    @Override
    public void showInAppError(@DrawableRes int iconRes, final String message, String btnText,
                               android.view.View.OnClickListener clickListener, int errorType,
                               int autoDismissTimeSecs) {
        if (Utils.isEmptyOrWhitespace(message) || errorTextView == null)
            return;

        errorImageView.setImageResource(iconRes);
        errorTextView.setText(message);
        if (!Utils.isEmptyOrWhitespace(btnText)) {
            errorButton.setText(btnText);
            errorButton.setVisibility(View.VISIBLE);
        } else {
            errorButton.setVisibility(View.GONE);
        }

        errorView.setOnClickListener(clickListener);
        errorView.setVisibility(View.VISIBLE);
        errorView.setTag(errorType);

        if (autoDismissTimeSecs > 0) {
            if (inAppErrorTimer != null) {
                inAppErrorTimer.cancel();
            }

            errorView.setTag(message);
            inAppErrorTimer = new CountDownTimer(autoDismissTimeSecs * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    if (errorView != null && message.equals(errorView.getTag().toString())) {
                        errorView.setVisibility(View.GONE);
                    }
                }
            };

            inAppErrorTimer.start();
        } else {
            errorView.setTag(null);
        }
    }

    @Override
    public int getInAppErrorType() {
        try {
            return (Integer) errorView.getTag();
        } catch (Exception e) {
            return UIService.ERROR_TYPE_NONE;
        }
    }

    @Override
    public void hideInAppError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                errorView.setVisibility(View.GONE);
                if (inAppErrorTimer != null) {
                    inAppErrorTimer.cancel();
                }
            }
        });
    }

    @Override
    public void navigateToHome() {
        UIService.getInstance().hideKeyboard();
        if (isFragmentInHistory(HomeFragment.class.getName())) {
            showFragmentFromHistory(HomeFragment.class.getName());
        } else {
            clearBackStack();
            addFragment(new HomeFragment(), true);
        }

        showLeftNavButton();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_left_nav_button:
                navigateToHome();
                break;
        }
    }

    @SuppressWarnings("unused")
    public int getContentAreaWidth() {
        return rootView.getWidth();
    }

    public int getFullHeight() {
        return rootView.getHeight();
    }

    public int getContentAreaHeight() {
        return rootView.getHeight() - navigationBar.getHeight();
    }

    @SuppressWarnings("unused")
    public int getNavigationBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.nav_bar_height);
    }

    @Override
    public void setNavBarBgColor(final int color) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                navigationBar.setBackgroundColor(color);
            }
        });
    }


    @Override
    public void onFragmentInteraction(String title) {
        getSupportActionBar().setTitle(title);
    }
}