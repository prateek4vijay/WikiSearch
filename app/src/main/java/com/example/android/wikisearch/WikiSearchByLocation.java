package com.example.android.wikisearch;

import java.util.List;

public class WikiSearchByLocation {


    /**
     * batchcomplete : true
     * query : {"geosearch":[{"pageid":18618509,"ns":0,"title":"Wikimedia Foundation","lat":37.7891838,"lon":-122.4033522,"dist":0,"primary":true},{"pageid":42936625,"ns":0,"title":"Foxcroft Building","lat":37.789166666667,"lon":-122.40333333333,"dist":2.5,"primary":true},{"pageid":9293658,"ns":0,"title":"One Montgomery Tower","lat":37.7891,"lon":-122.4033,"dist":10.4,"primary":true},{"pageid":2608926,"ns":0,"title":"San Francisco Mechanics' Institute","lat":37.788844,"lon":-122.403042,"dist":46.6,"primary":true},{"pageid":41234993,"ns":0,"title":"88 Kearny Street","lat":37.788688,"lon":-122.403477,"dist":56.2,"primary":true},{"pageid":9293369,"ns":0,"title":"McKesson Plaza","lat":37.7887,"lon":-122.4026,"dist":85.2,"primary":true},{"pageid":3183613,"ns":0,"title":"Hallidie Building","lat":37.790019444444,"lon":-122.40351944444,"dist":94.1,"primary":true},{"pageid":41173649,"ns":0,"title":"Ritz-Carlton Club and Residences","lat":37.788302,"lon":-122.403209,"dist":98.9,"primary":true},{"pageid":28665966,"ns":0,"title":"Hunter-Dulin Building","lat":37.7898,"lon":-122.4025,"dist":101.5,"primary":true},{"pageid":21723953,"ns":0,"title":"Gump's","lat":37.788583333333,"lon":-122.40427777778,"dist":105.2,"primary":true}]}
     */

    private boolean batchcomplete;
    private QueryBean query;

    public boolean isBatchcomplete() {
        return batchcomplete;
    }

    public void setBatchcomplete(boolean batchcomplete) {
        this.batchcomplete = batchcomplete;
    }

    public QueryBean getQuery() {
        return query;
    }

    public void setQuery(QueryBean query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "WikiSearchByLocation{" +
                "batchcomplete=" + batchcomplete +
                ", query=" + query +
                '}';
    }

    public static class QueryBean {
        private List<GeosearchBean> geosearch;

        public List<GeosearchBean> getGeosearch() {
            return geosearch;
        }

        public void setGeosearch(List<GeosearchBean> geosearch) {
            this.geosearch = geosearch;
        }

        public static class GeosearchBean {
            /**
             * pageid : 18618509
             * ns : 0
             * title : Wikimedia Foundation
             * lat : 37.7891838
             * lon : -122.4033522
             * dist : 0
             * primary : true
             */

            private int pageid;
            private int ns;
            private String title;
            private double lat;
            private double lon;
            private double dist;
            private boolean primary;

            public int getPageid() {
                return pageid;
            }

            public void setPageid(int pageid) {
                this.pageid = pageid;
            }

            public int getNs() {
                return ns;
            }

            public void setNs(int ns) {
                this.ns = ns;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLon() {
                return lon;
            }

            public void setLon(double lon) {
                this.lon = lon;
            }

            public double getDist() {
                return dist;
            }

            public void setDist(double dist) {
                this.dist = dist;
            }

            public boolean isPrimary() {
                return primary;
            }

            public void setPrimary(boolean primary) {
                this.primary = primary;
            }

            @Override
            public String toString() {
                return "GeosearchBean{" +
                        "pageid=" + pageid +
                        ", ns=" + ns +
                        ", title='" + title + '\'' +
                        ", lat=" + lat +
                        ", lon=" + lon +
                        ", dist=" + dist +
                        ", primary=" + primary +
                        '}';
            }
        }
    }
}
