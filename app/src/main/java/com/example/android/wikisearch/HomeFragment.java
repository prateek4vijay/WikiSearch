package com.example.android.wikisearch;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.wikisearch.utils.AppConstants;
import com.example.android.wikisearch.utils.FrameworkUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import framework.gps.GpsListener;
import framework.gps.GpsService;
import framework.ui.UIService;
import framework.util.Logger;
import framework.util.PermissionHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.wikisearch.utils.AppConstants.ACTION;
import static com.example.android.wikisearch.utils.AppConstants.QUERY;

public class HomeFragment extends BaseFragment implements GpsListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    public static final int PERMISSION_REQUEST = 1001;
    public static final String TAG = HomeFragment.class.getSimpleName();
    private ArticleFragment.OnFragmentInteractionListener mListener;
    Map<String, String> mMarkers = new HashMap<String, String>();
    private GoogleMap gMap;
    private MapView mapView;
    private RecyclerView mRecyclerView;
    private DataAdapter mAdapter;
    private MarkerOptions options = new MarkerOptions();

    @Override
    protected String getFragmentDisplayName() {
        return null;
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (ArticleFragment.OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mListener != null) {
            mListener.onFragmentInteraction("WikiSearch");
        }
        mapView = (MapView) view.findViewById(R.id.map);
        MapsInitializer.initialize(getActivity());
        mapView.onCreate(savedInstanceState);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
        if (PermissionHelper.hasPermission(PermissionHelper.getAllPermissions())) {
            if (Utils.isLocationEnabled()) {

            } else {
                Utils.checkLocationSettings();
            }
        } else {
            PermissionHelper.requestAllPermissions(this, PERMISSION_REQUEST);
        }
        if (gMap == null) {
            Logger.d(TAG, "map async called");
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onGpsPointReceived(final Location location) {
        UIService.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (getActivity() == null || isRemoving()) {
                    return;
                }
                if (location == null || gMap == null) {
                    return;
                }
                float currZoom = gMap.getCameraPosition().zoom;
                if (currZoom < 13)
                    currZoom = 15;
                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), currZoom);
                gMap.animateCamera(camera);
            }
        });
        String gscoord = location.getLatitude() + "|" + location.getLongitude();
        loadJSON(gscoord);
    }

    private void loadJSON(String query) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put(AppConstants.ACTION, AppConstants.QUERY);
        queryMap.put(AppConstants.LIST, AppConstants.GEOSEARCH);
        queryMap.put(AppConstants.GSCOORD, query);
        queryMap.put(AppConstants.GSRADIUS, AppConstants.GSRADIUSVALUE);
        queryMap.put(AppConstants.GSLIMIT, AppConstants.GSlimitVALUE);
        queryMap.put(AppConstants.FORMAT, AppConstants.JSON);
        queryMap.put(AppConstants.FORMATVERSION, AppConstants.FORMATVERSIONVALUE);
        Call<WikiSearchByLocation> call = FrameworkUtils.initRetrofit().getSearchListByLocation(queryMap);
        call.enqueue(new Callback<WikiSearchByLocation>() {
            @Override
            public void onResponse(Call<WikiSearchByLocation> call, Response<WikiSearchByLocation> response) {

                Logger.d(TAG, "response called");
                if (response.body() != null) {
                    WikiSearchByLocation jsonResponse = response.body();
                    if (jsonResponse != null) {
                        plotMarkers(jsonResponse);
                    }
                }

            }

            @Override
            public void onFailure(Call<WikiSearchByLocation> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void plotMarkers(WikiSearchByLocation jsonResponse) {

        WikiSearchByLocation.QueryBean queryBean = jsonResponse.getQuery();
        if (queryBean == null) {
            return;
        }
        List<WikiSearchByLocation.QueryBean.GeosearchBean> geosearchBeanList = queryBean.getGeosearch();
        if (geosearchBeanList == null || geosearchBeanList.isEmpty()) {
            return;
        }
        if (gMap == null) {
            return;
        }
        for (WikiSearchByLocation.QueryBean.GeosearchBean bean : geosearchBeanList) {
            LatLng latLng = new LatLng(bean.getLat(), bean.getLon());
            options.position(latLng);
            options.title(bean.getTitle());
            Marker marker = gMap.addMarker(options);
            mMarkers.put(marker.getId(), bean.getPageid() + "-" + bean.getTitle());
        }
    }


    @Override
    public void onError(String error) {

    }

    @Override
    public void onStop() {
        GpsService.getInstance().removeGpsListener(this);
        gMap = null;
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Logger.d(TAG, "map ready called");
        gMap = googleMap;
        gMap.getUiSettings().setCompassEnabled(false);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);
        gMap.getUiSettings().setMapToolbarEnabled(false);
        gMap.setOnMarkerClickListener(this);
        gMap.getUiSettings().setZoomControlsEnabled(false);
        gMap.getUiSettings().setTiltGesturesEnabled(true);
        gMap.getUiSettings().setMapToolbarEnabled(false);
        gMap.setMyLocationEnabled(true);
        GpsService.getInstance().addGpsListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if ((mMarkers != null && mMarkers.containsKey(marker.getId()))) {
            String pageId = mMarkers.get(marker.getId());
            String[] splitStr = pageId.split("-");
            UIService.getInstance().addFragment(ArticleFragment.getInstance(splitStr[1], splitStr[0]), true);
            return true;
        }
        return false;

    }


    private void initViews() {
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

    }


    private void loadJsonByName(String query) {

        Map<String, String> queryMap = new HashMap<>();
        queryMap.put(AppConstants.ACTION, AppConstants.QUERY);
        queryMap.put(AppConstants.FORMAT, AppConstants.JSON);
        queryMap.put(AppConstants.PROP, AppConstants.PAGEIMAGES_PAGETERMS);
        queryMap.put(AppConstants.GENERATOR, AppConstants.PREFIXSEARCH);
        queryMap.put(AppConstants.REDIRECTS, AppConstants.REDIRECTSVALUE);
        queryMap.put(AppConstants.FORMATVERSION, AppConstants.FORMATVERSIONVALUE);
        queryMap.put(AppConstants.PIPROP, AppConstants.THUMBNAIL);
        queryMap.put(AppConstants.PITHUMBSIZE,AppConstants. PITHUMBSIZE_VALUE);
        queryMap.put(AppConstants.WBPTTERMS, AppConstants.DESCRIPTION);
        queryMap.put(AppConstants.GPSSEARCH, query);
        queryMap.put(AppConstants.GPSLIMIT, AppConstants.GPSLIMIT_VALUE);
        Call<WikiSearchByName> call = FrameworkUtils.initRetrofit().getSearchListByName(queryMap);
        call.enqueue(new Callback<WikiSearchByName>() {
            @Override
            public void onResponse(Call<WikiSearchByName> call, Response<WikiSearchByName> response) {
                if (response.body() != null) {
                    WikiSearchByName jsonResponse = response.body();
                    if (jsonResponse != null) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mapView.setVisibility(View.GONE);
                        final List<WikiSearchByName.QueryBean.PagesBean> arrayList = jsonResponse.getQuery().getPages();
                        mAdapter = new DataAdapter(getContext(), new RecyclerViewClickListener() {
                            @Override
                            public void onClick(View view, int position) {
                                String title = arrayList.get(position).getTitle();
                                String pageId = String.valueOf(arrayList.get(position).getPageid());
                                UIService.getInstance().addFragment(ArticleFragment.getInstance(title, pageId), true);

                            }
                        }, arrayList);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }

            }

            @Override
            public void onFailure(Call<WikiSearchByName> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        rxJavaSearchwithDebounce(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void rxJavaSearchwithDebounce(SearchView searchView) {
        RxSearch.fromSearchView(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String text) throws Exception {
                        if (text.isEmpty()) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull String query) throws Exception {
                        loadJsonByName(query);
                    }
                });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.switchToMap:
                mRecyclerView.setVisibility(View.GONE);
                mapView.setVisibility(View.VISIBLE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
