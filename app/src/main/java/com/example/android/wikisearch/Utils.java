package com.example.android.wikisearch;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import framework.ui.AppStateListener;
import framework.ui.UIService;
import framework.ui.WikiApplication;
import framework.util.Logger;

public class Utils {
    public final static int PLAY_SERVICE_GPS_REQUEST_CODE = 29843;
    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";
    private static final String TAG = "Utils";
    private static final Object LOCK = new Object();

    private static String capitalize(String str) {
        if (isEmptyOrWhitespace(str)) {
            return "";
        }

        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

    public static boolean isEmptyOrWhitespace(String string) {
        return (string == null || "".equals(string.trim()));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("deprecation")
    public static boolean isLocationEnabled() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            String providers = Secure
                    .getString(WikiApplication.getAppContext().getContentResolver(),
                            Secure.LOCATION_PROVIDERS_ALLOWED);
            return (!isEmptyOrWhitespace(providers) && providers
                    .contains(LocationManager.GPS_PROVIDER));
        } else {
            final int locationMode;
            try {
                locationMode = Secure.getInt(WikiApplication.getAppContext().getContentResolver(),
                        Secure.LOCATION_MODE);
            } catch (SettingNotFoundException e) {
                Logger.e(TAG, "Setting Not Found Exception ", e);
                return false;
            }
            switch (locationMode) {
                case Secure.LOCATION_MODE_BATTERY_SAVING:
                case Secure.LOCATION_MODE_HIGH_ACCURACY:
                case Secure.LOCATION_MODE_SENSORS_ONLY:
                    return true;
                case Secure.LOCATION_MODE_OFF:
                default:
                    return false;
            }
        }
    }

    public static String getStringFromJson(JsonObject jsonObject, String key) {
        if (jsonObject == null || isEmptyOrWhitespace(key) || !jsonObject.has(key) || jsonObject
                .get(key).isJsonNull()) {
            return null;
        }

        JsonElement element = jsonObject.get(key);
        if (element.isJsonArray()) {
            return element.toString();
        } else {
            return element.getAsString();
        }
    }

    public static OnTouchListener getImageViewStateTouchListenr() {
        return new OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ImageView view = (ImageView) v;
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        view.getDrawable().setColorFilter(0x77000000,
                                android.graphics.PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        view.getDrawable().clearColorFilter();
                        view.invalidate();
                        break;
                }

                return false;
            }
        };
    }

    public static void showGpsErrorPrompt(final Activity activity,
                                          OnClickListener cancelListener) {
        if (activity == null)
            return;

        UIService.getInstance()
                .showAlert(R.string.gps_not_found_err_title, R.string.gps_not_found_err_msg,
                        R.string.gps_error_settings_redirect,
                        new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                activity.startActivity(new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                            }
                        }, R.string.gps_error_settings_cancel, cancelListener);
    }

    public static void showHomeFragment() {
        UIService.getInstance().clearBackStack();
        UIService.getInstance().addFragment(new HomeFragment());
    }

    public static void checkLocationSettings() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        Logger.d(TAG, "Check Location settings start.");
        SettingsClient settingsClient = LocationServices.getSettingsClient(WikiApplication.getAppContext());
        Task<LocationSettingsResponse> result = settingsClient.checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);


                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            Logger.d(TAG, "Location Service Error, Resolution Required.");
                            Activity activity = UIService.getInstance().getActivity();
                            ResolvableApiException resolvable = null;
                            if (UIService.getInstance().getActivity() != null && AppStateListener.isAppVisible()) {

                                try {
                                    resolvable = (ResolvableApiException) exception;
                                    resolvable.startResolutionForResult(activity, PLAY_SERVICE_GPS_REQUEST_CODE);
                                } catch (Exception e) {
                                    Logger.e(TAG, "Location Service Error, Can't show dialog", e);

                                }
                            } else {
                                Logger.e(TAG, "Location Service Error, Activity is null.");

                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            resolvable = (ResolvableApiException) exception;
                            Logger.d(TAG, "Location Service Error, Resolution not found.");

                            break;
                    }
                }
            }
        });
    }

    public static boolean isPageContainHyperlink() {
        Pattern p = Pattern.compile(URL_REGEX);
        Matcher m = p.matcher("example.com");//replace with string to compare
        if (m.find()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isDataConnectionOn() {
        try {
            ConnectivityManager connectionManager = (ConnectivityManager) WikiApplication.
                    getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectionManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        } catch (Exception e) {
            Logger.d("CommunicationUtils", "Error checking data connectivity", e);
            return false;
        }
    }
}