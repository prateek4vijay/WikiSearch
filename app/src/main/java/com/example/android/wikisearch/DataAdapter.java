package com.example.android.wikisearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    Context mContext;
    RecyclerViewClickListener listener;
    private List<WikiSearchByName.QueryBean.PagesBean> mArrayList = new ArrayList<>();

    public DataAdapter(Context mContext, RecyclerViewClickListener listener, List<WikiSearchByName.QueryBean.PagesBean> arrayList) {
        this.mContext = mContext;
        this.listener = listener;
        mArrayList = arrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        final ViewHolder mViewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        WikiSearchByName.QueryBean.PagesBean.ThumbnailBean thumbnailBean = mArrayList.get(i).getThumbnail();
        WikiSearchByName.QueryBean.PagesBean.TermsBean termsBean = mArrayList.get(i).getTerms();
        viewHolder.title.setText(mArrayList.get(i).getTitle());
        if (termsBean != null && termsBean.getDescription() != null) {
            viewHolder.description.setText(mArrayList.get(i).getTerms().getDescription().get(0));
        }
        if (thumbnailBean != null) {
            Picasso.get().load(thumbnailBean.getSource()).into(viewHolder.icon);
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList != null ? mArrayList.size() : 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description;
        private ImageView icon;

        public ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            icon = (ImageView) view.findViewById(R.id.icon);

        }
    }

}